ARG BUILD_IMAGE=mcr.microsoft.com/windows/servercore:ltsc2019
ARG BASE_IMAGE=mcr.microsoft.com/windows/nanoserver:1809
FROM $BUILD_IMAGE as installer-env

ENV APPVEYOR_BUILD_AGENT_VERSION=7.0.2417

COPY ./scripts/Windows ./scripts

USER ContainerAdministrator

# Install Chocolatey
RUN powershell -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

RUN choco install -y powershell-core && \
    choco install -y 7zip.install && \
    choco install -y git.install && \
    choco install -y hg && \
    choco install -y svn

#####
# Install .Vs2019
#####
RUN powershell ./scripts/install_cmake.ps1
RUN powershell ./scripts/install_vs2019.ps1
RUN powershell ./scripts/install_vdproj_vs2019.ps1
RUN powershell ./scripts/install_boost_1.71.0_vs2019.psl
RUN powershell ./scripts/install_winsdk_81.ps1
RUN powershell ./scripts/install_wdk_1809.ps1
RUN powershell ./scripts/install_dxsdk.ps1
RUN powershell ./scripts/install_wix311.ps1
RUN powershell ./scripts/install_wix_toolset_vs2019.ps1
RUN powershell ./scripts/install_llvm_extension_vs2019.ps1
RUN powershell ./scripts/install_vcpkg.ps1



#####
# Install .JDK
#####
RUN powershell ./scripts/install_java_7_8.ps1
RUN powershell ./scripts/install_maven.ps1


#####
# Install .OpenSSL
#####
RUN powershell ./scripts/install_openssl.ps1



#####
# Install .NodeJs
#####
RUN powershell ./scripts/install_nodejs_latest.ps1


#####
# Install .Cloud
#####
RUN powershell ./scripts/install_aws_cli.ps1
RUN powershell ./scripts/install_azure_cli.ps1
RUN powershell ./scripts/install_gcloud_sdk.ps1


#####
# Install .DataBase
#####
RUN powershell ./scripts/install_postgres10.ps1
RUN powershell ./scripts/install_mysql.ps1
RUN powershell ./scripts/install_erlang.ps1
RUN powershell ./scripts/install_mongo.ps1


#####
# Install .WebBrower
#####
RUN powershell ./scripts/install_chrome_browser.ps1
RUN powershell ./scripts/install_selenium_drivers.ps1


#####
# Install .Develop Tools
#####
RUN powershell ./scripts/install_ruby.ps1
RUN powershell ./scripts/install_miniconda.ps1
RUN powershell ./scripts/install_python.ps1
RUN powershell ./scripts/install_msys64.ps1












