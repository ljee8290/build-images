ARG BUILD_IMAGE=mcr.microsoft.com/windows/servercore:ltsc2019
ARG BASE_IMAGE=mcr.microsoft.com/windows/nanoserver:1809
FROM $BUILD_IMAGE as installer-env

ENV APPVEYOR_BUILD_AGENT_VERSION=7.0.2417

COPY ./scripts/Windows ./scripts

USER ContainerAdministrator

# Install Chocolatey
RUN powershell -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

RUN choco install -y powershell-core && \
    choco install -y 7zip.install && \
    choco install -y git && \
    choco install -y hg && \
    choco install -y svn

RUN powershell ./scripts/install_appveyor_build_agent_docker.ps1

# RUN powershell ./scripts/install_vs2019.ps1




# Download the Build Tools bootstrapper outside of the PATH.
ADD https://aka.ms/vs/16/release/vs_Enterprise.exe C:\\TEMP\\vs_Enterprise.exe

RUN C:\\TEMP\\vs_Enterprise.exe --wait --quiet --norestart --nocache --installPath C:\BuildTools \
####
#   VC++ Components
####
    --add Component.Android.NDK.R16B \
    --add Component.Android.SDK25.Private \
    --add Component.Android.SDK28 \
    --add Component.MDD.Android \
    --add Component.Microsoft.VisualStudio.RazorExtension \
    --add Component.Microsoft.VisualStudio.Web.AzureFunctions \
    --add Component.Microsoft.Web.LibraryManager \
    --add Component.OpenJDK \
    --add Component.Xamarin \
    --add Component.Xamarin.RemotedSimulator \
    --add Microsoft.Component.Azure.DataLake.Tools \
    --add Microsoft.Component.MSBuild \
    --add Microsoft.Component.NetFX.Native \
    --add Microsoft.Component.VC.Runtime.UCRTSDK \
    --add Microsoft.ComponentGroup.Blend \

    || IF "%ERRORLEVEL%"=="3010" EXIT 0
	
RUN C:\\TEMP\\vs_Enterprise.exe modify --wait --quiet --norestart --nocache --installPath C:\BuildTools \
####
#   UCRT
####
    --add Microsoft.Component.VC.Runtime.UCRTSDK \
	--add Microsoft.Net.Component.3.5.DeveloperTools \
    --add Microsoft.Net.Component.4.5.1.TargetingPack \
    --add Microsoft.Net.Component.4.5.2.TargetingPack \
    --add Microsoft.Net.Component.4.5.TargetingPack \
    --add Microsoft.Net.Component.4.6.1.SDK \
    --add Microsoft.Net.Component.4.6.1.TargetingPack \
    --add Microsoft.Net.Component.4.6.2.SDK \
    --add Microsoft.Net.Component.4.6.2.TargetingPack \
    --add Microsoft.Net.Component.4.6.TargetingPack \
    --add Microsoft.Net.Component.4.7.1.SDK \
    --add Microsoft.Net.Component.4.7.1.TargetingPack \
    --add Microsoft.Net.Component.4.7.2.SDK \
    --add Microsoft.Net.Component.4.7.2.TargetingPack \
    --add Microsoft.Net.Component.4.7.SDK \
    --add Microsoft.Net.Component.4.7.TargetingPack \
    --add Microsoft.Net.Component.4.8.SDK \
    --add Microsoft.Net.Component.4.8.TargetingPack \
    --add Microsoft.Net.Component.4.TargetingPack \
####
    || IF "%ERRORLEVEL%"=="3010" EXIT 0
	
RUN C:\\TEMP\\vs_Enterprise.exe modify --wait --quiet --norestart --nocache --installPath C:\BuildTools \
####
#   .Net
####
    --add Microsoft.VisualStudio.Component.ManagedDesktop.Core \
    --add Microsoft.VisualStudio.Component.ManagedDesktop.Prerequisites \
    --add Microsoft.VisualStudio.Component.Merq \
    --add Microsoft.VisualStudio.Component.MonoDebugger \
    --add Microsoft.VisualStudio.Component.MSODBC.SQL \
    --add Microsoft.VisualStudio.Component.MSSQL.CMDLnUtils \
    --add Microsoft.VisualStudio.Component.NuGet \
    --add Microsoft.VisualStudio.Component.PortableLibrary \
    --add Microsoft.VisualStudio.Component.Roslyn.Compiler \
    --add Microsoft.VisualStudio.Component.Roslyn.LanguageServices \
    --add Microsoft.VisualStudio.Component.Sharepoint.Tools \
    --add Microsoft.VisualStudio.Component.SQL.ADAL \
    --add Microsoft.VisualStudio.Component.SQL.CLR \
    --add Microsoft.VisualStudio.Component.SQL.DataSources \
    --add Microsoft.VisualStudio.Component.SQL.LocalDB.Runtime \
    --add Microsoft.VisualStudio.Component.SQL.SSDT \
    --add Microsoft.VisualStudio.Component.TeamOffice \
    --add Microsoft.VisualStudio.Component.TextTemplating \
    --add Microsoft.VisualStudio.Component.TypeScript.3.6 \
    --add Microsoft.VisualStudio.Component.VC.14.20.ATL \
    --add Microsoft.VisualStudio.Component.VC.14.20.ATL.Spectre \
    --add Microsoft.VisualStudio.Component.VC.14.20.CLI.Support \
    --add Microsoft.VisualStudio.Component.VC.14.20.MFC \
    --add Microsoft.VisualStudio.Component.VC.14.20.MFC.Spectre \
    --add Microsoft.VisualStudio.Component.VC.14.20.x86.x64 \
    --add Microsoft.VisualStudio.Component.VC.14.20.x86.x64.Spectre \
    --add Microsoft.VisualStudio.Component.VC.14.21.ARM \
    --add Microsoft.VisualStudio.Component.VC.14.21.ARM.Spectre \
    --add Microsoft.VisualStudio.Component.VC.14.21.ARM64 \
    --add Microsoft.VisualStudio.Component.VC.14.21.ARM64.Spectre \
    --add Microsoft.VisualStudio.Component.VC.14.21.ATL \
    --add Microsoft.VisualStudio.Component.VC.14.21.ATL.Spectre \
    --add Microsoft.VisualStudio.Component.VC.14.21.CLI.Support \
    --add Microsoft.VisualStudio.Component.VC.14.21.MFC \
    --add Microsoft.VisualStudio.Component.VC.14.21.MFC.Spectre \
    --add Microsoft.VisualStudio.Component.VC.14.21.x86.x64 \
    --add Microsoft.VisualStudio.Component.VC.14.21.x86.x64.Spectre \
    --add Microsoft.VisualStudio.Component.VC.14.22.ATL \
    --add Microsoft.VisualStudio.Component.VC.14.22.ATL.ARM \
    --add Microsoft.VisualStudio.Component.VC.14.22.ATL.ARM.Spectre \
    --add Microsoft.VisualStudio.Component.VC.14.22.ATL.ARM64 \
    --add Microsoft.VisualStudio.Component.VC.14.22.ATL.ARM64.Spectre \
    --add Microsoft.VisualStudio.Component.VC.14.22.ATL.Spectre \
    --add Microsoft.VisualStudio.Component.VC.14.22.CLI.Support \
    --add Microsoft.VisualStudio.Component.VC.14.22.MFC \
    --add Microsoft.VisualStudio.Component.VC.14.22.MFC.ARM \
    --add Microsoft.VisualStudio.Component.VC.14.22.MFC.ARM.Spectre \
    --add Microsoft.VisualStudio.Component.VC.14.22.MFC.ARM64 \
    --add Microsoft.VisualStudio.Component.VC.14.22.MFC.ARM64.Spectre \
    --add Microsoft.VisualStudio.Component.VC.14.22.MFC.Spectre \
    --add Microsoft.VisualStudio.Component.VC.14.22.x86.x64 \
    --add Microsoft.VisualStudio.Component.VC.14.22.x86.x64.Spectre \
    --add Microsoft.VisualStudio.Component.VC.140 \
    --add Microsoft.VisualStudio.Component.VC.ATL \
    --add Microsoft.VisualStudio.Component.VC.ATL.ARM \
    --add Microsoft.VisualStudio.Component.VC.ATL.ARM.Spectre \
    --add Microsoft.VisualStudio.Component.VC.ATL.ARM64 \
    --add Microsoft.VisualStudio.Component.VC.ATL.ARM64.Spectre \
    --add Microsoft.VisualStudio.Component.VC.ATL.Spectre \
    --add Microsoft.VisualStudio.Component.VC.ATLMFC \
    --add Microsoft.VisualStudio.Component.VC.ATLMFC.Spectre \
    --add Microsoft.VisualStudio.Component.VC.CLI.Support \
    --add Microsoft.VisualStudio.Component.VC.CMake.Project \
    --add Microsoft.VisualStudio.Component.VC.CoreIde \
    --add Microsoft.VisualStudio.Component.VC.DiagnosticTools \
    --add Microsoft.VisualStudio.Component.VC.Llvm.Clang \
    --add Microsoft.VisualStudio.Component.VC.Llvm.ClangToolset \
    --add Microsoft.VisualStudio.Component.VC.MFC.ARM \
    --add Microsoft.VisualStudio.Component.VC.MFC.ARM.Spectre \
    --add Microsoft.VisualStudio.Component.VC.MFC.ARM64 \
    --add Microsoft.VisualStudio.Component.VC.MFC.ARM64.Spectre \
    --add Microsoft.VisualStudio.Component.VC.Redist.14.Latest \
    --add Microsoft.VisualStudio.Component.VC.Runtimes.ARM.Spectre \
    --add Microsoft.VisualStudio.Component.VC.Runtimes.ARM64.Spectre \
    --add Microsoft.VisualStudio.Component.VC.Runtimes.x86.x64.Spectre \
    --add Microsoft.VisualStudio.Component.VC.TestAdapterForBoostTest \
    --add Microsoft.VisualStudio.Component.VC.TestAdapterForGoogleTest \
    --add Microsoft.VisualStudio.Component.VC.Tools.ARM \
    --add Microsoft.VisualStudio.Component.VC.Tools.ARM64 \
    --add Microsoft.VisualStudio.Component.VC.Tools.x86.x64 \
    --add Microsoft.VisualStudio.Component.VC.v141.ATL \
    --add Microsoft.VisualStudio.Component.VC.v141.ATL.Spectre \
    --add Microsoft.VisualStudio.Component.VC.v141.CLI.Support \
    --add Microsoft.VisualStudio.Component.VC.v141.MFC \
    --add Microsoft.VisualStudio.Component.VC.v141.MFC.Spectre \
    --add Microsoft.VisualStudio.Component.VC.v141.x86.x64 \
    --add Microsoft.VisualStudio.Component.VC.v141.x86.x64.Spectre \
    --add Microsoft.VisualStudio.Component.VSSDK \
    || IF "%ERRORLEVEL%"=="3010" EXIT 0

RUN C:\\TEMP\\vs_Enterprise.exe modify --wait --quiet --norestart --nocache --installPath C:\BuildTools \
####
#    ARM | Arm64
####
    --add Microsoft.VisualStudio.ComponentGroup.Azure.CloudServices \
    --add Microsoft.VisualStudio.ComponentGroup.Azure.Prerequisites \
    --add Microsoft.VisualStudio.ComponentGroup.Azure.ResourceManager.Tools \
    --add Microsoft.VisualStudio.ComponentGroup.AzureFunctions \
    --add Microsoft.VisualStudio.ComponentGroup.MSIX.Packaging \
    --add Microsoft.VisualStudio.ComponentGroup.NativeDesktop.Core \
    --add Microsoft.VisualStudio.ComponentGroup.UWP.NetCoreAndStandard \
    --add Microsoft.VisualStudio.ComponentGroup.UWP.Support \
    --add Microsoft.VisualStudio.ComponentGroup.UWP.Xamarin \
    --add Microsoft.VisualStudio.ComponentGroup.VisualStudioExtension.Prerequisites \
    --add Microsoft.VisualStudio.ComponentGroup.Web \
    --add Microsoft.VisualStudio.ComponentGroup.Web.CloudTools \
    --add Microsoft.VisualStudio.ComponentGroup.WebToolsExtensions \
    --add Microsoft.VisualStudio.ComponentGroup.WebToolsExtensions.CMake \
    --add Microsoft.VisualStudio.ComponentGroup.WebToolsExtensions.TemplateEngine \
    --add Microsoft.VisualStudio.Workload.Azure \
    --add Microsoft.VisualStudio.Workload.CoreEditor \
    --add Microsoft.VisualStudio.Workload.Data \
    --add Microsoft.VisualStudio.Workload.ManagedDesktop \
    --add Microsoft.VisualStudio.Workload.NativeDesktop \
    --add Microsoft.VisualStudio.Workload.NativeMobile \
    --add Microsoft.VisualStudio.Workload.NetCoreTools \
    --add Microsoft.VisualStudio.Workload.NetCrossPlat \
    --add Microsoft.VisualStudio.Workload.NetWeb \
    --add Microsoft.VisualStudio.Workload.Office \
    --add Microsoft.VisualStudio.Workload.Universal \
    --add Microsoft.VisualStudio.Workload.VisualStudioExtension \
    --add Microsoft.VisualStudio.Component.VC.CoreBuildTools \
    --add Microsoft.VisualStudio.Component.Windows10SDK \
    || IF "%ERRORLEVEL%"=="3010" EXIT 0

RUN C:\\TEMP\\vs_Enterprise.exe modify --wait --quiet --norestart --nocache --installPath C:\BuildTools \
####
#   Windows SDK
####
    --add Microsoft.VisualStudio.Component.Wcf.Tooling \
    --add Microsoft.VisualStudio.Component.Web \
    --add Microsoft.VisualStudio.Component.WebDeploy \
    --add Microsoft.VisualStudio.Component.Windows10SDK.16299 \
    --add Microsoft.VisualStudio.Component.Windows10SDK.17134 \
    --add Microsoft.VisualStudio.Component.Windows10SDK.17763 \
    --add Microsoft.VisualStudio.Component.Windows10SDK.18362 \
    --add Microsoft.VisualStudio.Component.WinXP \
    --add Microsoft.VisualStudio.Component.Workflow \
	--add Microsoft.Net.ComponentGroup.4.6.2.DeveloperTools \
    --add Microsoft.Net.ComponentGroup.DevelopmentPrerequisites \
    --add Microsoft.Net.ComponentGroup.TargetingPacks.Common \
    --add Microsoft.NetCore.Component.DevelopmentTools \
    --add Microsoft.NetCore.Component.SDK \
    --add Microsoft.NetCore.Component.Web \
    --add Microsoft.VisualStudio.Component.AppInsights.Tools \
    --add Microsoft.VisualStudio.Component.AspNet45 \
    --add Microsoft.VisualStudio.Component.Azure.AuthoringTools \
    --add Microsoft.VisualStudio.Component.Azure.ClientLibs \
    --add Microsoft.VisualStudio.Component.Azure.Compute.Emulator \
    --add Microsoft.VisualStudio.Component.Azure.ResourceManager.Tools \
    --add Microsoft.VisualStudio.Component.Azure.ServiceFabric.Tools \
    --add Microsoft.VisualStudio.Component.Azure.Storage.Emulator \
    --add Microsoft.VisualStudio.Component.Azure.Waverton \
    --add Microsoft.VisualStudio.Component.Azure.Waverton.BuildTools \
    --add Microsoft.VisualStudio.Component.CloudExplorer \
    --add Microsoft.VisualStudio.Component.Common.Azure.Tools \
    --add Microsoft.VisualStudio.Component.CoreEditor \
    --add Microsoft.VisualStudio.Component.Debugger.JustInTime \
    --add Microsoft.VisualStudio.Component.DiagnosticTools \
    --add Microsoft.VisualStudio.Component.DockerTools \
    --add Microsoft.VisualStudio.Component.EntityFramework \
    --add Microsoft.VisualStudio.Component.FSharp \
    --add Microsoft.VisualStudio.Component.FSharp.Desktop \
    --add Microsoft.VisualStudio.Component.FSharp.WebTemplates \
    --add Microsoft.VisualStudio.Component.Graphics \
    --add Microsoft.VisualStudio.Component.Graphics.Tools \
    --add Microsoft.VisualStudio.Component.IISExpress \
    --add Microsoft.VisualStudio.Component.IntelliCode \
    --add Microsoft.VisualStudio.Component.JavaScript.Diagnostics \
    --add Microsoft.VisualStudio.Component.JavaScript.TypeScript \
    || IF "%ERRORLEVEL%"=="3010" EXIT 0


RUN powershell ./scripts/install_vdproj_vs2019.ps1